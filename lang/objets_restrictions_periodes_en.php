<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/objets_restrictions_periodes?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_duree_label' => 'Duration:',
	'champ_duree_minimale_label' => 'Minimum duration:',
	'champ_jour_debut_label' => 'Beginning day:',
	'champ_jour_fin_label' => 'End day:',
	'champ_type_label' => 'Type:',
	'choix_duree' => 'Duration',
	'choix_jours' => 'Days',

	// E
	'erreur_duree' => 'Minimum duration of @duree@ @entite_duree@',
	'erreur_jours' => 'The @date@ must be a @jour@!',

	// O
	'objets_restrictions_periodes_titre' => 'Objects restrictions periods',

	// T
	'titre_page_configurer_objets_restrictions_periodes' => 'Objects restrictions periods'
);
